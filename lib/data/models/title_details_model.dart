class TitleDetailsModel {
  int id;
  String name;
  String overview;
  DateTime releaseDate;
  num voteAverage;
  String homePage;
  String runtime;
  List<String> genres;
  String coverUrl;
  String posterUrl;
  List<CommentModel> comments;

  TitleDetailsModel({
    required this.id,
    required this.name,
    required this.overview,
    required this.releaseDate,
    required this.voteAverage,
    required this.homePage,
    required this.runtime,
    required this.genres,
    required this.coverUrl,
    required this.posterUrl,
    required this.comments,
  });

  factory TitleDetailsModel.fromJson(Map<String, dynamic> json) {
    return TitleDetailsModel(
      id: json['id'],
      name: json['name'],
      overview: json['overview'],
      releaseDate: DateTime.parse(json['release_date']),
      voteAverage: json['vote_average'],
      homePage: json['homepage'],
      runtime: json['runtime'],
      genres: List<String>.from(
          json['genres'].map((value) => value['name']).toList()),
      coverUrl: json['cover_url'],
      posterUrl: json['poster_url'],
      comments: List<CommentModel>.from(
          json['comments'].map((c) => CommentModel.fromJason(c)).toList()),
    );
  }
}

class CommentModel {
  int id;
  String text;
  DateTime date;

  CommentModel({required this.id, required this.text, required this.date});

  factory CommentModel.fromJason(Map<String, dynamic> json) {
    return CommentModel(
      id: json['id'],
      text: json['text'] ?? '',
      date: DateTime.parse(json['date']),
    );
  }
}
