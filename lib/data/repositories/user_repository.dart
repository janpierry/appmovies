import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:my_movies_list/data/exceptions/user_already_exists_exception.dart';
import 'package:my_movies_list/data/exceptions/user_not_found_exception.dart';
import 'package:my_movies_list/data/models/user_model.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/data/services/http_service.dart';
import 'package:my_movies_list/shared/strings.dart';

class UserRepository implements UserRepositoryInterface {
  final HttpService _http;
  final _baseUrl = Strings.movie_api_url;
  final _secureStorage = FlutterSecureStorage();
  UserModel? user;

  UserRepository(this._http);

  @override
  Future<void> clearSession() async {
    await _secureStorage.delete(key: 'AUTH_TOKEN');
  }

  @override
  Future<String?> getToken() {
    return _secureStorage.read(key: 'AUTH_TOKEN');
  }

  @override
  Future<UserModel?> getUser() async {
    if (user == null) {
      final token = await getToken();
      final uri = '$_baseUrl/auth/me';
      final headers = {'Authorization': 'Bearer $token'};
      final response = await _http.getRequest(uri, headers: headers);
      user = UserModel.fromJson(response.content!);
    }

    return user;
  }

  @override
  Future<bool> isLogged() async {
    final token = await getToken();
    return token != null;
  }

  @override
  Future<UserModel?> login(String email, String password) async {
    final uri = '$_baseUrl/auth/login';
    final body = {'email': email, 'password': password};
    final response = await _http.postRequest(uri, body);

    if (response.success) {
      var token = response.content!['token'];

      await saveToken(token);
      return getUser();
    }

    if (response.statusCode == 400) {
      throw UserNotFoundException();
    }

    throw Exception('Falha ao realizar login');
  }

  @override
  Future<UserModel?> register(
      String email, String password, String name) async {
    final uri = '$_baseUrl/auth/register';
    final body = {'email': email, 'password': password, 'name': name};

    var response = await _http.postRequest(uri, body);

    if (response.success) {
      var token = response.content!['token'];

      await saveToken(token);
      return getUser();
    }

    if (response.statusCode == 400) {
      throw UserAlreadyExistsException();
    }

    throw Exception('Falha ao cadastrar usuário');
  }

  @override
  Future<void> saveToken(String token) async {
    await _secureStorage.write(key: 'AUTH_TOKEN', value: token);
  }

  @override
  Future<List<UserModel>> getUsers() async {
    final uri = '$_baseUrl/users';
    final token = await getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final response = await _http.getRequest(uri, headers: headers);

    List<dynamic> data = response.content!['data'];

    return List<UserModel>.from(data.map((j) => UserModel.fromJson(j)));
  }
}
