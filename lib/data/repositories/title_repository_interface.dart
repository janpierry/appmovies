import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';

abstract class TitleRepositoryInterface {
  Future<List<TitleModel>> getMovieList({Map<String, dynamic>? params});

  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params});

  Future<TitleDetailsModel> getTitleDetails(int id, {bool isTvShow = false});

  Future<List<TitleModel>> getTitleRecomendation(int titleId,
      {bool isTvShow = false});

  Future<bool> saveComment(int titleId, String text, {bool isTvShow = false});

  Future<bool> removeComment(int titleId, int commentId,
      {bool isTvShow = false});

  Future<List<CommentModel>> getTitleComments(int id, {bool isTvShow = false});

  Future<bool> saveTitleRate(int titleId, int rate, {bool isTvShow = false});

  Future<int?> getTitleRate(int titleId, {bool isTvShow = false});

  Future<List<TitleModel>> getRatedTitles();

  Future<List<TitleModel>> getRatedTitlesFromUser(String userId);

  Future<Map<String, dynamic>?> getPaginatedTitles(
      {Map<String, dynamic>? params});
}
