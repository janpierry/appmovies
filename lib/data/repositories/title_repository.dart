import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/data/services/http_service.dart';
import 'package:my_movies_list/locator.dart';

class TitleRepository implements TitleRepositoryInterface {
  final HttpService _httpService;
  final _baseUrl = 'https://xbfuvqstcb.execute-api.us-east-1.amazonaws.com/dev';
  final _userRepository = getIt.get<UserRepositoryInterface>();

  TitleRepository(this._httpService);

  @override
  Future<List<TitleModel>> getMovieList({Map<String, dynamic>? params}) async {
    String uri = '$_baseUrl/movies';
    var response = await _httpService.getRequest(uri, params: params);
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      return List<TitleModel>.from(data.map((j) => TitleModel.fromJson(j)));
    }

    return [];
  }

  @override
  Future<List<TitleModel>> getTvList({Map<String, dynamic>? params}) async {
    String uri = '$_baseUrl/tv';
    var response = await _httpService.getRequest(uri, params: params);
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      return List<TitleModel>.from(data.map((j) => TitleModel.fromJson(j)));
    }

    return [];
  }

  @override
  Future<TitleDetailsModel> getTitleDetails(int id,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    String uri = '$_baseUrl/$titleType/$id';
    var response = await _httpService.getRequest(uri);
    if (response.success) {
      dynamic data = response.content!;

      return TitleDetailsModel.fromJson(data);
    }

    throw Exception('Erro interno no servidor');
  }

  @override
  Future<List<TitleModel>> getTitleRecomendation(int movieId,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    String uri = '$_baseUrl/$titleType/$movieId/recommendations';

    var response = await _httpService.getRequest(uri);
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      return List<TitleModel>.from(data.map((j) => TitleModel.fromJson(j)));
    }

    return [];
  }

  @override
  Future<bool> removeComment(int titleId, int commentId,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/$commentId/comment';
    final token = await _userRepository.getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final result = await _httpService.deleteRequest(uri, headers: headers);

    return result.success;
  }

  @override
  Future<bool> saveComment(int titleId, String text,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/comment';
    final token = await _userRepository.getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final result = await _httpService.postRequest(uri, {'comment': text},
        headers: headers);

    return result.success;
  }

  @override
  Future<int?> getTitleRate(int titleId, {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/rate';
    final token = await _userRepository.getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final result = await _httpService.getRequest(uri, headers: headers);

    if (result.statusCode == 404) return null;

    return result.content!['rate'];
  }

  @override
  Future<bool> saveTitleRate(int titleId, int rate,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    final uri = '$_baseUrl/$titleType/$titleId/rate';
    final token = await _userRepository.getToken();
    final headers = {'Authorization': 'Bearer $token'};
    final result =
        await _httpService.postRequest(uri, {'rate': rate}, headers: headers);

    return result.success;
  }

  @override
  Future<List<TitleModel>> getRatedTitles() async {
    var user = await _userRepository.getUser();
    var id = user != null ? user.id : '';
    return getRatedTitlesFromUser(id);
  }

  @override
  Future<List<TitleModel>> getRatedTitlesFromUser(String userId) async {
    String uri = '$_baseUrl/users/$userId/titles-rated';
    final token = await _userRepository.getToken();
    final headers = {'Authorization': 'Bearer $token'};
    var response = await _httpService.getRequest(uri, headers: headers);
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      return List<TitleModel>.from(data.map((j) => TitleModel.fromJson(j)));
    }

    return [];
  }

  @override
  Future<List<CommentModel>> getTitleComments(int id,
      {bool isTvShow = false}) async {
    final titleType = isTvShow ? 'tv' : 'movies';
    String uri = '$_baseUrl/$titleType/$id/comments';
    var response = await _httpService.getRequest(uri);
    if (response.success) {
      dynamic data = response.content!['data'];

      return List<CommentModel>.from(
          data.map((c) => CommentModel.fromJason(c)).toList());
    }

    throw Exception('Erro interno no servidor');
  }

  @override
  Future<Map<String, dynamic>?> getPaginatedTitles(
      {Map<String, dynamic>? params}) async {
    String uri = '$_baseUrl/movies';
    var response = await _httpService.getRequest(uri, params: params);
    if (response.success) {
      List<dynamic> data = response.content!['data'];

      var movies =
          List<TitleModel>.from(data.map((j) => TitleModel.fromJson(j)));

      var count = response.content!['count'];

      return {'movies': movies, 'count': count};
    }

    return null;
  }
}
