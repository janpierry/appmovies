import 'package:flutter/material.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/pages/rated_titles_page.dart';
import 'package:my_movies_list/ui/pages/register_page.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';
import 'package:my_movies_list/ui/pages/splash_page.dart';
import 'package:my_movies_list/ui/pages/title_details_page.dart';
import 'package:my_movies_list/ui/pages/user_list_page.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My Movies List',
      debugShowCheckedModeBanner: false,
      initialRoute: SplashPage.name,
      routes: {
        LoginPage.name: (_) => LoginPage(),
        HomePage.name: (_) => const HomePage(),
        SearchPage.name: (_) => SearchPage(),
        TitleDetailsPage.name: (_) => TitleDetailsPage(),
        RegisterPage.name: (_) => RegisterPage(),
        RatedTitlesPage.name: (_) => RatedTitlesPage(),
        SplashPage.name: (_) => SplashPage(),
        UserListPage.name: (_) => UserListPage(),
      },
    );
  }
}
