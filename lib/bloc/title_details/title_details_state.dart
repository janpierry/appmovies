part of 'title_details_cubit.dart';

abstract class TitleDetailsState {}

class TitleDetailsLoadingState implements TitleDetailsState {}

class TitleDetailsLoadedState implements TitleDetailsState {
  final TitleDetailsModel titleDetails;

  TitleDetailsLoadedState(this.titleDetails);
}

class TitleDetailsErrorState implements TitleDetailsState {
  final String message;

  TitleDetailsErrorState(this.message);
}
