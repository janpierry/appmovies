import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

part 'title_details_state.dart';

class TitleDetailsCubit extends Cubit<TitleDetailsState> {
  final TitleRepositoryInterface _titleRepository;

  TitleDetailsCubit(TitleDetailsState initialState, this._titleRepository)
      : super(initialState);

  Future<void> getTitleDetails(int titleId, bool isTvShow) async {
    emit(TitleDetailsLoadingState());
    try {
      final titleDetails =
          await _titleRepository.getTitleDetails(titleId, isTvShow: isTvShow);
      emit(TitleDetailsLoadedState(titleDetails));
    } catch (e) {
      emit(TitleDetailsErrorState(e.toString()));
    }
  }
}
