part of 'title_list_cubit.dart';

abstract class TitleListState {}

class TitleListLoadingAllState implements TitleListState {}

class TitleListLoadingState implements TitleListState {
  final TitleType type;

  TitleListLoadingState(this.type);
}

class TitleListLoadedState implements TitleListState {
  final TitleType type;
  final List<TitleModel> titles;

  TitleListLoadedState(this.titles, this.type);
}

class TitleListErrorState implements TitleListState {
  final String message;

  TitleListErrorState(this.message);
}
