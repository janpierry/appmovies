import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

part 'title_list_state.dart';

class TitleListCubit extends Cubit<TitleListState> {
  final TitleRepositoryInterface _titleRepository;

  TitleListCubit(TitleListState initialState, this._titleRepository)
      : super(initialState);

  Future<void> getTitles(List<TitleType> titleTypes) async {
    for (final titleType in titleTypes) {
      List<TitleModel> titles;
      emit(TitleListLoadingState(titleType));
      try {
        titles = titleType.isTvShow
            ? await _titleRepository.getTvList(params: titleType.fetchParams)
            : await _titleRepository.getMovieList(
                params: titleType.fetchParams);
        emit(TitleListLoadedState(titles, titleType));
      } catch (e) {
        emit(TitleListErrorState(e.toString()));
      }
    }
  }
}

class TitleType {
  String label;
  Map<String, dynamic> fetchParams;
  bool isTvShow;

  TitleType(this.label, this.fetchParams, this.isTvShow);
}
