import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/user_model.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';

part 'user_list_state.dart';

class UserListCubit extends Cubit<UserListState> {
  final UserRepositoryInterface _userRepository;

  UserListCubit(UserListState initialState, this._userRepository)
      : super(initialState);

  Future<void> getUsers() async {
    emit(UserListLoadingState());
    try {
      final users = await _userRepository.getUsers();
      emit(UserListLoadedState(users));
    } catch (e) {
      emit(UserListErrorState(e.toString()));
    }
  }
}
