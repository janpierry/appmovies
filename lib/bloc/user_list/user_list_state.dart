part of 'user_list_cubit.dart';

abstract class UserListState {}

class UserListInitialState implements UserListState {}

class UserListLoadingState implements UserListState {}

class UserListLoadedState implements UserListState {
  final List<UserModel> users;

  UserListLoadedState(this.users);
}

class UserListErrorState implements UserListState {
  final String message;

  UserListErrorState(this.message);
}
