import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/exceptions/user_already_exists_exception.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  final UserRepositoryInterface _userRepository;

  RegisterCubit(RegisterState initialState, this._userRepository)
      : super(initialState);

  Future<void> register(String email, String password, String name) async {
    emit(RegisterLoadingState());
    try {
      await _userRepository.register(email, password, name);
      emit(RegisterSuccessState());
    } on UserAlreadyExistsException {
      emit(RegisterErrorState('Usuário já existe'));
    } on Exception catch (e) {
      emit(RegisterErrorState(e.toString()));
    }
  }
}
