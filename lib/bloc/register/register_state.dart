part of 'register_cubit.dart';

abstract class RegisterState {}

class RegisterInitialState implements RegisterState {}

class RegisterLoadingState implements RegisterState {}

class RegisterSuccessState implements RegisterState {}

class RegisterErrorState implements RegisterState {
  final String message;

  RegisterErrorState(this.message);
}
