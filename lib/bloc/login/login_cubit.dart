import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/exceptions/user_not_found_exception.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';

part 'login_state.dart';

class LoginCubit extends Cubit<LoginState> {
  final UserRepositoryInterface _userRepository;

  LoginCubit(LoginState initialState, this._userRepository)
      : super(initialState);

  Future<void> login(String email, String password) async {
    emit(LoginLoadingState());
    try {
      await _userRepository.login(email, password);
      emit(LoginSuccessState());
    } on UserNotFoundException {
      emit(LoginErrorState('Usuário não encontrado'));
    } on Exception catch (e) {
      emit(LoginErrorState(e.toString()));
    }
  }
}
