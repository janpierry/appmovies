import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

part 'search_state.dart';

class SearchCubit extends Cubit<SearchState> {
  var currentPage = 0;
  var lastSearch = '';
  int? totalPages;
  List<TitleModel> titles = [];
  final TitleRepositoryInterface _titleRepository;

  SearchCubit(SearchState initialState, this._titleRepository)
      : super(initialState);

  Future<void> searchTitles(String text) async {
    if (text != lastSearch) {
      clear();
      lastSearch = text;
    }
    currentPage++;
    if (currentPage == 1) emit(SearchLoadingState());
    if (totalPages != null && currentPage > totalPages!) {
      return;
    }
    try {
      var data = await _titleRepository
          .getPaginatedTitles(params: {'name': text, 'page': currentPage});
      if (data != null) {
        titles.addAll(data['movies']);
        totalPages = (data['count'] / 10).ceil();
        emit(SearchLoadedState(titles));
      }
    } catch (e) {
      emit(SearchErrorState(e.toString()));
    }
  }

  void clear() {
    currentPage = 0;
    totalPages = null;
    titles = [];
  }
}
