part of 'search_cubit.dart';

abstract class SearchState {}

class SearchInitialState implements SearchState {}

class SearchLoadingState implements SearchState {}

class SearchLoadedState implements SearchState {
  final List<TitleModel> titles;

  SearchLoadedState(this.titles);
}

class SearchErrorState implements SearchState {
  final String message;

  SearchErrorState(this.message);
}
