import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';

part 'splash_state.dart';

class SplashCubit extends Cubit<SplashState> {
  final UserRepositoryInterface _userRepository;

  SplashCubit(SplashState initialState, this._userRepository)
      : super(initialState);

  Future<void> checkUser() async {
    await Future.delayed(const Duration(seconds: 2));
    final isLogged = await _userRepository.isLogged();

    isLogged ? emit(SplashState.userLogged) : emit(SplashState.userNotLogged);
  }
}
