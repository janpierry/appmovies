import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

part 'title_details_recomendations_state.dart';

class TitleDetailsRecommendationsCubit
    extends Cubit<TitleDetailsRecommendationsState> {
  final TitleRepositoryInterface _titleRepository;

  TitleDetailsRecommendationsCubit(
      TitleDetailsRecommendationsLoadingState initialState,
      this._titleRepository)
      : super(initialState);

  Future<void> getTitleDetailsRecommendations(
      int titleId, bool isTvShow) async {
    emit(TitleDetailsRecommendationsLoadingState());
    try {
      final recommendations = await _titleRepository
          .getTitleRecomendation(titleId, isTvShow: isTvShow);
      emit(TitleDetailsRecommendationsLoadedState(recommendations));
    } catch (e) {
      emit(TitleDetailsRecommendationsErrorState(e.toString()));
    }
  }
}
