part of 'title_details_recomendations_cubit.dart';

abstract class TitleDetailsRecommendationsState {}

class TitleDetailsRecommendationsLoadingState
    implements TitleDetailsRecommendationsState {}

class TitleDetailsRecommendationsLoadedState
    implements TitleDetailsRecommendationsState {
  final List<TitleModel> titles;

  TitleDetailsRecommendationsLoadedState(this.titles);
}

class TitleDetailsRecommendationsErrorState
    implements TitleDetailsRecommendationsState {
  final String message;

  TitleDetailsRecommendationsErrorState(this.message);
}
