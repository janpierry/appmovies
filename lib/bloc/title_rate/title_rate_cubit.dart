import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

part 'title_rate_state.dart';

class TitleRateCubit extends Cubit<TitleRateState> {
  final TitleRepositoryInterface _titleRepository;

  TitleRateCubit(TitleRateState initialState, this._titleRepository)
      : super(initialState);

  Future<void> getTitleRate(int titleId, bool isTvShow) async {
    emit(TitleRateLoadingState());
    try {
      final rate =
          await _titleRepository.getTitleRate(titleId, isTvShow: isTvShow);
      emit(TitleRateLoadedState(titleId, rate));
    } catch (e) {
      emit(TitleRateErrorState(e.toString()));
    }
  }

  Future<void> rateTitle(int titleId, int rate, bool isTvShow) async {
    emit(TitleRateLoadingState());
    try {
      await _titleRepository.saveTitleRate(titleId, rate, isTvShow: isTvShow);
      emit(TitleRateLoadedState(titleId, rate));
    } catch (e) {
      emit(TitleRateErrorState(e.toString()));
    }
  }
}
