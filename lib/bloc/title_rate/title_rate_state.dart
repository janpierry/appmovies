part of 'title_rate_cubit.dart';

abstract class TitleRateState {}

class TitleRateInitialState implements TitleRateState {}

class TitleRateLoadingState implements TitleRateState {}

class TitleRateLoadedState implements TitleRateState {
  final int titleId;
  final int? rating;

  TitleRateLoadedState(this.titleId, this.rating);
}

class TitleRateErrorState implements TitleRateState {
  final String message;

  TitleRateErrorState(this.message);
}
