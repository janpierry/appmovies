import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

part 'title_details_comments_state.dart';

class TitleDetailsCommentsCubit extends Cubit<TitleDetailsCommentsState> {
  final TitleRepositoryInterface _titleRepository;

  TitleDetailsCommentsCubit(
      TitleDetailsCommentsState initialState, this._titleRepository)
      : super(initialState);

  Future<void> saveComment(int titleId, String comment, bool isTvShow) async {
    emit(TitleDetailsCommentsLoadingState());
    await _titleRepository.saveComment(titleId, comment, isTvShow: isTvShow);
    getTitleComments(titleId, isTvShow);
  }

  Future<void> removeComment(int titleId, int commentId, bool isTvShow) async {
    emit(TitleDetailsCommentsLoadingState());
    await _titleRepository.removeComment(titleId, commentId,
        isTvShow: isTvShow);
    getTitleComments(titleId, isTvShow);
  }

  Future<void> getTitleComments(int titleId, bool isTvShow) async {
    try {
      final comments =
          await _titleRepository.getTitleComments(titleId, isTvShow: isTvShow);
      emit(TitleDetailsCommentsLoadedState(comments));
    } catch (e) {
      emit(TitleDetailsCommentsErrorState(e.toString()));
    }
  }
}
