part of 'title_details_comments_cubit.dart';

abstract class TitleDetailsCommentsState {}

class TitleDetailsCommentsLoadingState implements TitleDetailsCommentsState {}

class TitleDetailsCommentsLoadedState implements TitleDetailsCommentsState {
  final List<CommentModel> comments;

  TitleDetailsCommentsLoadedState(this.comments);
}

class TitleDetailsCommentsErrorState implements TitleDetailsCommentsState {
  final String message;

  TitleDetailsCommentsErrorState(this.message);
}
