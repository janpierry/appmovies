import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';

part 'rated_titles_state.dart';

class RatedTitlesCubit extends Cubit<RatedTitlesState> {
  final TitleRepositoryInterface _titleRepository;

  RatedTitlesCubit(RatedTitlesState initialState, this._titleRepository)
      : super(initialState);

  Future<void> getRatedTitles(String? userId) async {
    emit(RatedTitlesLoadingState());
    try {
      final titles = userId == null
          ? await _titleRepository.getRatedTitles()
          : await _titleRepository.getRatedTitlesFromUser(userId);
      emit(RatedTitlesLoadedState(titles));
    } catch (e) {
      emit(RatedTitlesErrorState(e.toString()));
    }
  }
}
