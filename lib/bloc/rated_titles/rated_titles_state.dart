part of 'rated_titles_cubit.dart';

abstract class RatedTitlesState {}

class RatedTitlesLoadingState implements RatedTitlesState {}

class RatedTitlesLoadedState implements RatedTitlesState {
  final List<TitleModel> titles;

  RatedTitlesLoadedState(this.titles);
}

class RatedTitlesErrorState implements RatedTitlesState {
  final String message;

  RatedTitlesErrorState(this.message);
}
