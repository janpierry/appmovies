import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/title_list/title_list_cubit.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/tab-pages/widgets/catalog_title_view.dart';

class SeriesTabPage extends StatelessWidget {
  const SeriesTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titleTypes = [
      TitleType('Filmes de aventura', {'genre': 10759}, true),
      TitleType('Séries de comédia', {'genre': 35}, true),
      TitleType('Filmes de animação', {'genre': 16}, true),
      TitleType('Filmes de romance', {'genre': 10749}, true),
    ];

    return BlocProvider(
      create: (context) => TitleListCubit(
          TitleListLoadingAllState(), getIt.get<TitleRepositoryInterface>())
        ..getTitles(titleTypes),
      child: CatalogTitleView(
        titles: titleTypes,
      ),
    );
  }
}
