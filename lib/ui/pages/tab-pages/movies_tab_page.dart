import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/title_list/title_list_cubit.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/tab-pages/widgets/catalog_title_view.dart';

class MoviesTabPage extends StatelessWidget {
  const MoviesTabPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final titleTypes = [
      TitleType('Filmes de ação', {'genre': 28}, false),
      TitleType('Filmes de animação', {'genre': 16}, false),
      TitleType('Filmes de terror', {'genre': 27}, false),
      TitleType('Filmes de ficção científica', {'genre': 878}, false),
    ];

    return BlocProvider(
      create: (context) => TitleListCubit(
          TitleListLoadingAllState(), getIt.get<TitleRepositoryInterface>())
        ..getTitles(titleTypes),
      child: CatalogTitleView(titles: titleTypes),
    );
  }
}
