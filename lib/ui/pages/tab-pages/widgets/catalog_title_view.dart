import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/title_list/title_list_cubit.dart';
import 'package:my_movies_list/ui/widgets/title_carousel.dart';

class CatalogTitleView extends StatelessWidget {
  final List<TitleType> titles;
  const CatalogTitleView({Key? key, required this.titles}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: titles
            .map(
              (t) => BlocBuilder<TitleListCubit, TitleListState>(
                  buildWhen: (oldState, newState) {
                if (newState is TitleListLoadingState)
                  return newState.type.label == t.label;
                if (newState is TitleListLoadedState)
                  return newState.type.label == t.label;
                return true;
              }, builder: (context, state) {
                if (state is TitleListLoadingState) {
                  return Center(child: CircularProgressIndicator());
                } else if (state is TitleListLoadedState) {
                  return Column(
                    children: [
                      SizedBox(height: 16.0),
                      TitleCarousel(titles: state.titles, label: t.label),
                    ],
                  );
                } else {
                  return Container(
                    padding: EdgeInsets.all(20),
                    child: Center(
                        child: Text(
                      (state as TitleListErrorState).message,
                      style: TextStyle(fontSize: 16),
                    )),
                  );
                }
              }),
            )
            .toList(),
      ),
    );
  }
}
