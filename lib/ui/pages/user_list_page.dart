import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/user_list/user_list_cubit.dart';
import 'package:my_movies_list/data/models/user_model.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/rated_titles_page.dart';

class UserListPage extends StatelessWidget {
  static const name = 'user-list-page';
  const UserListPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => UserListCubit(
          UserListInitialState(), getIt.get<UserRepositoryInterface>())
        ..getUsers(),
      child: UserListView(),
    );
  }
}

class UserListView extends StatelessWidget {
  const UserListView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Lista de usuários')),
      body: BlocBuilder<UserListCubit, UserListState>(
        builder: (context, state) {
          if (state is UserListLoadingState) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is UserListLoadedState) {
            return _buildUserList(context, state.users);
          } else {
            return Container(
              padding: EdgeInsets.all(20),
              child: Center(
                  child: Text(
                (state as UserListErrorState).message,
                style: TextStyle(fontSize: 16),
              )),
            );
          }
        },
      ),
    );
  }

  Widget _buildUserList(BuildContext context, List<UserModel> users) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...users.map((u) => InkWell(
                onTap: () {
                  Navigator.of(context)
                      .pushNamed(RatedTitlesPage.name, arguments: u.id);
                },
                child: ListTile(
                  title: Text(u.name),
                  leading: CircleAvatar(
                    child: Text(u.name[0]),
                  ),
                ),
              ))
        ],
      ),
    );
  }
}
