import 'package:flutter/material.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';
import 'package:my_movies_list/ui/pages/rated_titles_page.dart';
import 'package:my_movies_list/ui/pages/search_page.dart';
import 'package:my_movies_list/ui/pages/tab-pages/main_tab_page.dart';
import 'package:my_movies_list/ui/pages/tab-pages/movies_tab_page.dart';
import 'package:my_movies_list/ui/pages/tab-pages/series_tab_page.dart';
import 'package:my_movies_list/ui/pages/user_list_page.dart';

class HomePage extends StatelessWidget {
  static const name = 'home-page';

  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          title: Text('Meu catálogo de filmes e séries'),
          bottom: TabBar(
            tabs: [
              Tab(child: Text('Principal')),
              Tab(child: Text('Filmes')),
              Tab(child: Text('Séries')),
            ],
          ),
          actions: [
            IconButton(
              onPressed: () => Navigator.pushNamed(context, SearchPage.name),
              icon: Icon(Icons.search),
            ),
            IconButton(
              onPressed: () =>
                  Navigator.pushNamed(context, RatedTitlesPage.name),
              icon: Icon(Icons.thumb_up_sharp),
            ),
            IconButton(
              onPressed: () => Navigator.pushNamedAndRemoveUntil(
                  context, LoginPage.name, (context) => false),
              icon: Icon(Icons.exit_to_app),
            ),
            IconButton(
              onPressed: () => Navigator.pushNamed(context, UserListPage.name),
              icon: Icon(Icons.people),
            ),
          ],
        ),
        body: TabBarView(
          physics: NeverScrollableScrollPhysics(),
          children: [
            MainTabPage(),
            MoviesTabPage(),
            SeriesTabPage(),
          ],
        ),
      ),
    );
  }
}
