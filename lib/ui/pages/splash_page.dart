import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/splash/splash_cubit.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/login_page.dart';

class SplashPage extends StatelessWidget {
  static const name = 'splash-page';

  const SplashPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SplashCubit(
          SplashState.checkingUser, getIt.get<UserRepositoryInterface>())
        ..checkUser(),
      child: SplashView(),
    );
  }
}

class SplashView extends StatelessWidget {
  SplashView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocListener<SplashCubit, SplashState>(
      listener: (context, state) {
        if (state == SplashState.userLogged) {
          Navigator.pushReplacementNamed(context, HomePage.name);
        } else if (state == SplashState.userNotLogged) {
          Navigator.pushReplacementNamed(context, LoginPage.name);
        }
      },
      child: Scaffold(
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
              Colors.white,
              Colors.blue,
            ]),
          ),
          child: Center(
            child: Icon(
              Icons.movie,
              size: 60.0,
            ),
          ),
        ),
      ),
    );
  }
}
