import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/register/register_cubit.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/widgets/mml_loading_button.dart';
import 'package:my_movies_list/ui/widgets/mml_text_form_field.dart';

class RegisterPage extends StatelessWidget {
  static const name = 'register-page';

  const RegisterPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => RegisterCubit(
          RegisterInitialState(), getIt.get<UserRepositoryInterface>()),
      child: RegisterView(),
    );
  }
}

class RegisterView extends StatelessWidget {
  final TextEditingController _emailController;
  final TextEditingController _passwordController;
  final TextEditingController _nameController;

  RegisterView({Key? key})
      : _emailController = TextEditingController(),
        _passwordController = TextEditingController(),
        _nameController = TextEditingController(),
        super(key: key);

  void _handleSuccess(BuildContext context) {
    Navigator.of(context)
        .pushNamedAndRemoveUntil(HomePage.name, (route) => false);
  }

  void _handleError(BuildContext context, String message) {
    ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: Text(message)));
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<RegisterCubit, RegisterState>(
      listener: (context, state) {
        if (state is RegisterSuccessState) _handleSuccess(context);
        if (state is RegisterErrorState) _handleError(context, state.message);
      },
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Preencha os campos para se cadastrar',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  SizedBox(height: 24.0),
                  MMLTextFormField(
                    controller: _emailController,
                    label: 'Email',
                    keyboardType: TextInputType.emailAddress,
                  ),
                  SizedBox(height: 20.0),
                  MMLTextFormField(
                    controller: _passwordController,
                    obscureText: true,
                    label: 'Senha',
                  ),
                  SizedBox(height: 20.0),
                  MMLTextFormField(
                    controller: _nameController,
                    label: 'Nome',
                  ),
                  SizedBox(height: 24.0),
                  BlocBuilder<RegisterCubit, RegisterState>(
                    builder: (context, state) {
                      return MMLLoadingButton(
                        isLoading: state is RegisterLoadingState,
                        label: 'Cadastrar',
                        onPressed: () => context.read<RegisterCubit>().register(
                              _emailController.text,
                              _passwordController.text,
                              _nameController.text,
                            ),
                      );
                    },
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
