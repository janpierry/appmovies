import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/title_details/title_details_cubit.dart';
import 'package:my_movies_list/bloc/title_details_comments/title_details_comments_cubit.dart';
import 'package:my_movies_list/bloc/title_details_recomendations/title_details_recomendations_cubit.dart';
import 'package:my_movies_list/bloc/title_rate/title_rate_cubit.dart';
import 'package:my_movies_list/data/models/title_details_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/mml_loading_button.dart';
import 'package:my_movies_list/ui/widgets/mml_text_form_field.dart';
import 'package:my_movies_list/ui/widgets/rate_buttons.dart';
import 'package:my_movies_list/ui/widgets/recomendations_carousel.dart';
import 'package:my_movies_list/ui/widgets/title_poster.dart';

class TitleDetailsPage extends StatelessWidget {
  static const name = 'title-details-page';
  const TitleDetailsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final arguments = ModalRoute.of(context)!.settings.arguments as Map;
    final titleId = arguments['id'] as int;
    final isTvShow = arguments['isTvShow'] as bool;
    final titleRepository = getIt.get<TitleRepositoryInterface>();

    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => TitleDetailsCubit(
            TitleDetailsLoadingState(),
            titleRepository,
          )..getTitleDetails(titleId, isTvShow),
        ),
        BlocProvider(
          create: (context) => TitleDetailsRecommendationsCubit(
            TitleDetailsRecommendationsLoadingState(),
            titleRepository,
          )..getTitleDetailsRecommendations(titleId, isTvShow),
        ),
        BlocProvider(
          create: (context) => TitleRateCubit(
            TitleRateLoadingState(),
            titleRepository,
          )..getTitleRate(titleId, isTvShow),
        ),
        BlocProvider(
          create: (context) => TitleDetailsCommentsCubit(
            TitleDetailsCommentsLoadingState(),
            titleRepository,
          )..getTitleComments(titleId, isTvShow),
        ),
      ],
      child: TitleDetailsView(
        titleId: titleId,
        isTvShow: isTvShow,
      ),
    );
  }
}

class TitleDetailsView extends StatelessWidget {
  final int titleId;
  final bool isTvShow;
  TitleDetailsView({Key? key, required this.titleId, required this.isTvShow})
      : super(key: key);

  final _commentController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              BlocBuilder<TitleDetailsCubit, TitleDetailsState>(
                builder: (context, state) {
                  if (state is TitleDetailsLoadingState) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (state is TitleDetailsLoadedState) {
                    return _buildDetails(state.titleDetails);
                  } else {
                    return Container(
                      padding: EdgeInsets.all(20),
                      child: Center(
                          child: Text(
                        (state as TitleDetailsErrorState).message,
                        style: TextStyle(fontSize: 16),
                      )),
                    );
                  }
                },
              ),
              SizedBox(height: 20.0),
              BlocBuilder<TitleDetailsRecommendationsCubit,
                  TitleDetailsRecommendationsState>(
                builder: (context, state) {
                  if (state is TitleDetailsRecommendationsLoadingState) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (state is TitleDetailsRecommendationsErrorState) {
                    return Container(
                      padding: EdgeInsets.all(20),
                      child: Center(
                          child: Text(
                        state.message,
                        style: TextStyle(fontSize: 16),
                      )),
                    );
                  } else {
                    return RecomendationsCarousel(
                      titles: (state as TitleDetailsRecommendationsLoadedState)
                          .titles,
                    );
                  }
                },
              ),
              SizedBox(height: 20.0),
              BlocBuilder<TitleRateCubit, TitleRateState>(
                builder: (context, state) {
                  if (state is TitleRateLoadingState) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else if (state is TitleRateLoadedState) {
                    return _buildRateSection(context, state.rating);
                  } else {
                    return Container(
                      padding: EdgeInsets.all(20),
                      child: Center(
                          child: Text(
                        (state as TitleRateErrorState).message,
                        style: TextStyle(fontSize: 16),
                      )),
                    );
                  }
                },
              ),
              SizedBox(height: 20.0),
              _buildCommentsHeader(context),
              BlocBuilder<TitleDetailsCommentsCubit, TitleDetailsCommentsState>(
                builder: (context, state) {
                  if (state is TitleDetailsCommentsLoadingState) {
                    return const Center(child: CircularProgressIndicator());
                  } else if (state is TitleDetailsCommentsLoadedState) {
                    return _buildComments(context, state.comments);
                  } else {
                    return Container(
                      padding: EdgeInsets.all(20),
                      child: Center(
                          child: Text(
                        (state as TitleDetailsCommentsErrorState).message,
                        style: TextStyle(fontSize: 16),
                      )),
                    );
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildDetails(TitleDetailsModel title) {
    return Column(
      children: [
        TitlePoster(url: title.coverUrl),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                '${title.name} (${title.releaseDate.year})',
                style: TextStyle(fontWeight: FontWeight.w500, fontSize: 18.0),
              ),
              const SizedBox(
                height: 20,
              ),
              Text(
                'Sinopse:',
                style: TextStyle(
                  fontWeight: FontWeight.w700,
                ),
              ),
              const SizedBox(
                height: 4.0,
              ),
              Text(
                title.overview,
                style: TextStyle(fontSize: 16.0),
              ),
              const SizedBox(
                height: 12.0,
              ),
              Text('Duração: ${title.runtime} min'),
              const SizedBox(
                height: 20.0,
              ),
              SizedBox(
                height: 40,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: title.genres.length,
                    itemBuilder: (context, index) {
                      return Row(
                        children: [
                          Chip(label: Text(title.genres[index])),
                          SizedBox(width: 8.0),
                        ],
                      );
                    }),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildRateSection(BuildContext context, int? rate) {
    final rateBool = rate == null
        ? null
        : rate == 1
            ? true
            : false;
    return RateButtons(
      onChanged: (value) => context
          .read<TitleRateCubit>()
          .rateTitle(titleId, value ? 1 : -1, isTvShow),
      value: rateBool,
    );
  }

  Widget _buildCommentsHeader(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12.0),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Expanded(
            child: MMLTextFormField(
                label: 'Comenário', controller: _commentController),
          ),
          SizedBox(width: 8.0),
          MMLLoadingButton(
            label: 'Comentar',
            onPressed: () async {
              context
                  .read<TitleDetailsCommentsCubit>()
                  .saveComment(titleId, _commentController.text, isTvShow);
              _commentController.clear();
            },
          )
        ],
      ),
    );
  }

  Widget _buildComments(BuildContext context, List<CommentModel> comments) {
    return Column(
      children: [..._buildCommentsList(context, comments)],
    );
  }

  List<Widget> _buildCommentsList(
      BuildContext context, List<CommentModel> comments) {
    return comments
        .map((c) => ListTile(
              title: Text(c.text),
              subtitle: Text(c.date.toString()),
              trailing: IconButton(
                onPressed: () async {
                  context
                      .read<TitleDetailsCommentsCubit>()
                      .removeComment(titleId, c.id, isTvShow);
                },
                icon: const Icon(
                  Icons.remove_circle,
                  color: Colors.red,
                ),
              ),
            ))
        .toList();
  }
}
