import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/rated_titles/rated_titles_cubit.dart';
import 'package:my_movies_list/bloc/title_rate/title_rate_cubit.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/rate_buttons.dart';
import 'package:my_movies_list/ui/widgets/title_poster.dart';

class RatedTitlesPage extends StatelessWidget {
  static const name = 'rated-titles-page';
  const RatedTitlesPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final userId = ModalRoute.of(context)!.settings.arguments as String?;
    final titleRepository = getIt.get<TitleRepositoryInterface>();

    return MultiBlocProvider(providers: [
      BlocProvider(
        create: (context) =>
            RatedTitlesCubit(RatedTitlesLoadingState(), titleRepository)
              ..getRatedTitles(userId),
      ),
      BlocProvider(
        create: (context) =>
            TitleRateCubit(TitleRateInitialState(), titleRepository),
      ),
    ], child: RatedTitlesView());
  }
}

class RatedTitlesView extends StatelessWidget {
  RatedTitlesView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Títulos classificados'),
      ),
      body: BlocBuilder<RatedTitlesCubit, RatedTitlesState>(
        builder: (context, state) {
          if (state is RatedTitlesLoadingState) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          } else if (state is RatedTitlesLoadedState) {
            return _buildTitleList(context, state.titles);
          } else {
            return Container(
              padding: EdgeInsets.all(20),
              child: Center(
                  child: Text(
                (state as RatedTitlesErrorState).message,
                style: TextStyle(fontSize: 16),
              )),
            );
          }
        },
      ),
    );
  }

  Widget _buildTitleList(BuildContext context, List<TitleModel> titles) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ...titles.map((t) => Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    height: 300,
                    child: _buildTitleCard(t),
                  ),
                  Expanded(
                      child: Column(
                    children: [
                      Text(
                        t.name,
                        textAlign: TextAlign.center,
                        style: TextStyle(fontSize: 20),
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      _buildRateSection(context, t.id, t.isTvShow),
                    ],
                  )),
                ],
              ))
        ],
      ),
    );
  }

  Widget _buildTitleCard(TitleModel title) {
    return TitlePoster(url: title.posterUrl);
  }

  Widget _buildRateSection(BuildContext context, int titleId, bool isTvShow) {
    context.read<TitleRateCubit>().getTitleRate(titleId, isTvShow);
    return BlocBuilder<TitleRateCubit, TitleRateState>(
      buildWhen: (oldState, newState) {
        return newState is TitleRateLoadedState && newState.titleId == titleId;
      },
      builder: (context, state) {
        if (state is TitleRateLoadingState) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else if (state is TitleRateLoadedState) {
          final rateBool = state.rating == null
              ? null
              : state.rating == 1
                  ? true
                  : false;
          return RateButtons(
            onChanged: (value) => context
                .read<TitleRateCubit>()
                .rateTitle(titleId, value ? 1 : -1, isTvShow),
            value: rateBool,
          );
        } else {
          return Container(
            padding: EdgeInsets.all(20),
            child: Center(
                child: Text(
              (state as TitleRateErrorState).message,
              style: TextStyle(fontSize: 16),
            )),
          );
        }
      },
    );
  }
}
