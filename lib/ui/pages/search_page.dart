import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/data/repositories/title_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/widgets/title_card.dart';
import 'package:my_movies_list/bloc/search/search_cubit.dart';

class SearchPage extends StatelessWidget {
  static const name = 'search-page';

  const SearchPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => SearchCubit(
          SearchInitialState(), getIt.get<TitleRepositoryInterface>()),
      child: SearchView(),
    );
  }
}

class SearchView extends StatefulWidget {
  SearchView({Key? key}) : super(key: key);

  @override
  State<SearchView> createState() => _SearchViewState();
}

class _SearchViewState extends State<SearchView> {
  final _searchController = TextEditingController();

  final _scrollController = ScrollController();

  @override
  void initState() {
    _scrollController.addListener(() {
      final maxScroll = _scrollController.position.maxScrollExtent;
      final currentScroll = _scrollController.position.pixels;
      if (maxScroll - currentScroll <= 150) {
        context.read<SearchCubit>().searchTitles(_searchController.text);
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: TextFormField(
          controller: _searchController,
          style: TextStyle(color: Colors.white),
          decoration: InputDecoration(
            hintText: 'Pesquise aqui...',
            hintStyle: TextStyle(color: Colors.white30),
            border: InputBorder.none,
          ),
        ),
        actions: [
          IconButton(
              onPressed: () => context
                  .read<SearchCubit>()
                  .searchTitles(_searchController.text),
              icon: Icon(Icons.search))
        ],
      ),
      body: BlocBuilder<SearchCubit, SearchState>(
        builder: (context, state) {
          if (state is SearchLoadingState) {
            return Center(child: CircularProgressIndicator());
          } else if (state is SearchLoadedState) {
            return GridView.count(
                controller: _scrollController,
                padding: const EdgeInsets.only(top: 8.0),
                mainAxisSpacing: 20.0,
                crossAxisSpacing: 20.0,
                crossAxisCount: 2,
                children: state.titles.map((t) => _buildTitleCard(t)).toList());
          } else if (state is SearchInitialState) {
            return Center(
              child: Text(
                'Pesquise por filmes e séries...',
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
              ),
            );
          } else {
            return Container(
              padding: EdgeInsets.all(20),
              child: Center(
                  child: Text(
                (state as SearchErrorState).message,
                style: TextStyle(fontSize: 16),
              )),
            );
          }
        },
      ),
    );
  }

  Widget _buildTitleCard(TitleModel title) {
    return Center(
      child: TitleCard(
          height: 160,
          titleModel: TitleCardModel(
            name: title.name,
            imageUrl: title.posterUrl,
          )),
    );
  }
}
