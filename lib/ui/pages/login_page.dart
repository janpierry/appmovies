import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:my_movies_list/bloc/login/login_cubit.dart';
import 'package:my_movies_list/data/repositories/user_repository_interface.dart';
import 'package:my_movies_list/locator.dart';
import 'package:my_movies_list/ui/pages/home_page.dart';
import 'package:my_movies_list/ui/pages/register_page.dart';
import 'package:my_movies_list/ui/widgets/mml_loading_button.dart';
import 'package:my_movies_list/ui/widgets/mml_text_form_field.dart';

class LoginPage extends StatelessWidget {
  static const name = 'login-page';

  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) =>
          LoginCubit(LoginInitialState(), getIt.get<UserRepositoryInterface>()),
      child: LoginView(),
    );
  }
}

void _handleSuccess(BuildContext context) {
  Navigator.of(context)
      .pushNamedAndRemoveUntil(HomePage.name, (route) => false);
}

void _handleError(BuildContext context, String message) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text(message)));
}

class LoginView extends StatelessWidget {
  LoginView({Key? key}) : super(key: key);

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocListener<LoginCubit, LoginState>(
      listener: (context, state) {
        if (state is LoginSuccessState) _handleSuccess(context);
        if (state is LoginErrorState) _handleError(context, state.message);
      },
      child: Scaffold(
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Text(
                    'Informe suas credenciais para começar!',
                    style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.w300,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  SizedBox(height: 24.0),
                  MMLTextFormField(
                    label: 'Email',
                    controller: _emailController,
                    keyboardType: TextInputType.emailAddress,
                  ),
                  SizedBox(height: 20.0),
                  MMLTextFormField(
                    obscureText: true,
                    label: 'Senha',
                    controller: _passwordController,
                  ),
                  SizedBox(height: 24.0),
                  BlocBuilder<LoginCubit, LoginState>(
                    builder: (context, state) {
                      bool processingLogin = false;
                      if (state is LoginLoadingState) {
                        processingLogin = true;
                      }
                      return MMLLoadingButton(
                        label: 'Entrar',
                        onPressed: () => context.read<LoginCubit>().login(
                              _emailController.text,
                              _passwordController.text,
                            ),
                        isLoading: processingLogin,
                      );
                    },
                  ),
                  SizedBox(height: 16.0),
                  Text(
                    'OU',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 12.0,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.pushNamed(context, RegisterPage.name);
                    },
                    child: Text('Criar minha conta'),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
