import 'package:flutter/material.dart';

class RateButtons extends StatelessWidget {
  final bool? value;
  final Function(bool)? onChanged;

  const RateButtons({
    this.value,
    this.onChanged,
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
          onPressed: () {
            if (onChanged != null) {
              onChanged!(true);
            }
          },
          icon: Icon(
            value == true ? Icons.thumb_up : Icons.thumb_up_outlined,
          ),
        ),
        IconButton(
          onPressed: () {
            if (onChanged != null) {
              onChanged!(false);
            }
          },
          icon: Icon(
            value == false ? Icons.thumb_down : Icons.thumb_down_outlined,
          ),
        ),
      ],
    );
  }
}
