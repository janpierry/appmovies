import 'package:flutter/material.dart';
import 'package:my_movies_list/ui/widgets/title_poster.dart';

class TitleCardModel {
  String name;
  String imageUrl;

  TitleCardModel({required this.name, required this.imageUrl});
}

class TitleCard extends StatelessWidget {
  final TitleCardModel titleModel;
  final Color backgroundColor;
  final double width;
  final double? height;

  const TitleCard({
    Key? key,
    required this.titleModel,
    this.backgroundColor = Colors.blue,
    this.width = 160.0,
    this.height,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(4.0),
      width: width,
      color: backgroundColor,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 4.0),
            child: TitlePoster(
              url: titleModel.imageUrl,
              width: width,
              height: height,
            ),
          ),
          Container(
            width: width,
            color: Theme.of(context).primaryColor,
            padding: const EdgeInsets.only(
              left: 8.0,
              bottom: 4.0,
              top: 4.0,
            ),
            child: Center(
              child: Text(
                titleModel.name,
                overflow: TextOverflow.ellipsis,
                style: const TextStyle(
                  color: Colors.white60,
                  fontSize: 13.0,
                  fontWeight: FontWeight.w700,
                ),
              ),
            ),
          ),
          // Stack(
          //   children: [
          //     TitlePoster(url: titleModel.imageUrl),
          //     Positioned(
          //       bottom: 0,
          //       left: 0,
          //       right: 0,
          //       child: Container(
          //         color: Theme.of(context).primaryColor,
          //         padding:
          //             const EdgeInsets.only(left: 8.0, bottom: 4.0, top: 4.0),
          //         child: Text(
          //           titleModel.name,
          //           textAlign: TextAlign.center,
          //           style: TextStyle(
          //             color: Colors.white60,
          //             fontSize: 13.0,
          //             fontWeight: FontWeight.w700,
          //           ),
          //         ),
          //       ),
          //     ),
          //   ],
          // ),
        ],
      ),
    );
  }
}
