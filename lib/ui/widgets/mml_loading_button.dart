import 'package:flutter/material.dart';

class MMLLoadingButton extends StatelessWidget {
  final String label;
  final bool isLoading;
  final VoidCallback onPressed;
  const MMLLoadingButton({
    Key? key,
    required this.label,
    this.isLoading = false,
    required this.onPressed,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40,
      child: isLoading
          ? Center(child: CircularProgressIndicator())
          : ElevatedButton(
              onPressed: onPressed,
              child: Text(label),
            ),
    );
  }
}
