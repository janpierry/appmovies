import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/ui/pages/title_details_page.dart';
import 'package:my_movies_list/ui/widgets/carousel.dart';
import 'package:my_movies_list/ui/widgets/title_poster.dart';

class RecomendationsCarousel extends StatelessWidget {
  final List<TitleModel> titles;

  const RecomendationsCarousel({Key? key, required this.titles})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (titles.isEmpty) {
      return Container(
        padding: EdgeInsets.all(20),
        child: Center(
            child: Text(
          'Sem títulos relacionados...',
          style: TextStyle(fontSize: 16),
        )),
      );
    }
    return Carousel(
      children: titles
          .map((t) => _buildTitleCard(title: t, context: context))
          .toList(),
    );
  }

  Widget _buildTitleCard(
      {required TitleModel title, required BuildContext context}) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, TitleDetailsPage.name,
            arguments: {'id': title.id, 'isTvShow': title.isTvShow});
      },
      child: Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: TitlePoster(
          width: 150.0,
          url: title.coverUrl,
        ),
      ),
    );
  }
}
