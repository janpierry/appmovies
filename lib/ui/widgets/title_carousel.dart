import 'package:flutter/material.dart';
import 'package:my_movies_list/data/models/title_model.dart';
import 'package:my_movies_list/ui/pages/title_details_page.dart';
import 'package:my_movies_list/ui/widgets/carousel.dart';
import 'package:my_movies_list/ui/widgets/title_card.dart';

class TitleCarousel extends StatelessWidget {
  final List<TitleModel> titles;
  final String label;

  TitleCarousel({
    Key? key,
    required this.titles,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Carousel(
        label: label,
        children: titles
            .map((t) => _buildTitleCard(title: t, context: context))
            .toList());
  }

  Widget _buildTitleCard(
      {required TitleModel title, required BuildContext context}) {
    return GestureDetector(
      onTap: () {
        Navigator.pushNamed(context, TitleDetailsPage.name,
            arguments: {'id': title.id, 'isTvShow': title.isTvShow});
      },
      child: Container(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: TitleCard(
              width: 120.0,
              titleModel: TitleCardModel(
                name: title.name,
                imageUrl: title.posterUrl,
              )),
        ),
      ),
    );
  }
}
