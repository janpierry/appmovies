import 'package:flutter/material.dart';

class CommentSection extends StatelessWidget {
  const CommentSection({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        _buildHeader(),
        _buildComments(),
      ],
    );
  }

  Widget _buildHeader() {
    return Container();
  }

  Widget _buildComments() {
    return Container();
  }
}
