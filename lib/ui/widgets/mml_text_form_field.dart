import 'package:flutter/material.dart';

class MMLTextFormField extends StatelessWidget {
  final String label;
  final TextEditingController? controller;
  final TextInputType? keyboardType;
  final bool obscureText;
  const MMLTextFormField({
    Key? key,
    required this.label,
    this.controller,
    this.keyboardType,
    this.obscureText = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      obscureText: this.obscureText,
      keyboardType: this.keyboardType,
      decoration: InputDecoration(
        labelText: this.label,
        border: OutlineInputBorder(),
      ),
    );
  }
}
